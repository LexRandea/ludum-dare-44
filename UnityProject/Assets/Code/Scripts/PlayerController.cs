﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private CharacterController controller;
    private MoneyShooter shooter;
    public float speed;
    public Transform moneySpawn;

    public MoneyWallet wallet;

    public Animator animator;

    public EndPanel endPanel;
    public CountdownClock clock;

    public Camera cam;
    public Transform body;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        shooter = moneySpawn.GetComponent<MoneyShooter>();
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Vector3 direction = hit.point - transform.position;
            direction.y = 0;
            body.transform.rotation = Quaternion.LookRotation(direction);
        }

        
        if (Input.GetMouseButtonDown(0)&&wallet.GetMoney()>shooter.coin.value)
        {
            animator.SetTrigger("Shooting");
            shooter.LaunchMoney();

        }

        if (Input.GetKey ("escape")) 
        {
            Application.Quit();
        }

        if (wallet.GetMoney()<=0)
        {
            Debug.Log("Bankrupt");
            endPanel.gameObject.SetActive(true);
            endPanel.Bankrupt();
        }

        Vector3 moveDirection = new Vector3(Input.GetAxis("Horizontal"), -transform.position.y, Input.GetAxis("Vertical"));
        transform.TransformDirection(moveDirection);
        moveDirection = moveDirection * speed * Time.deltaTime;

        if (Vector3.Magnitude(moveDirection)>0.01)
        {
            animator.SetBool("Running", true);
        }
        else
        {
            animator.SetBool("Running", false);
        }

        controller.Move(moveDirection);

    }
}
