﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tracker : MonoBehaviour
{

    public Transform target;
    public Transform origin;
    public float radius  = 42.0f;
    public float scaling = 200.0f;
    public RectTransform map;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if ((target != null) && (origin != null))
        {
            Vector3 vec = target.position - origin.position;
            vec.x = vec.x / scaling * radius;
            vec.y = vec.z / scaling * radius;
            vec.z = GetComponent<RectTransform>().position.z;
            
            if (vec.magnitude > radius)
            {
                vec = vec.normalized * radius;
            }

            vec = map.position + vec;
            GetComponent<RectTransform>().position = vec;
        }
    }
}
