﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RandomAI : MonoBehaviour {

    public int noise = 10;
    public NavMeshAgent agent;
    public MoneyShooter shooter;
    public Transform target;

    public MoneyWallet wallet;

    private float timeSinceLastShot = 0.0f;
    private float timeBetweenShots = 0.0f;

    public float maxTimeBetweenShots = 0.0f;
    public int minimalMoney = 100;

    void Start()
    {
        Random.InitState((int)(System.DateTime.Now.Second));
    }

    void Update()
    {
        if (DistanceToTarget()>500)
        {
            UpdateDestination();
        }
        if (DistanceToDestination()<50)
        {
            UpdateDestination();
        }   
        TryShooting();
    }

    void UpdateDestination()
    {
        Vector3 dist = target.position + (new Vector3(Random.Range(-noise, noise), 0, Random.Range(-noise, noise)));
        agent.SetDestination(dist);
    }


    void UpdateTimeBetweenShots()
    {
        timeBetweenShots = maxTimeBetweenShots * Random.Range(0.0f,1.0f);
    }

    float DistanceToTarget()
    {
        Vector3 pos1 = transform.position;
        pos1.y = 0.0f;

        Vector3 pos2 = target.position;
        pos2.y = 0.0f; 

        return Vector3.Distance(pos1,pos2);
    }

    float DistanceToDestination()
    {
        Vector3 pos1 =  transform.position;
        pos1.y = 0.0f;

        Vector3 pos2 = agent.pathEndPosition;
        pos2.y = 0.0f; 

        return Vector3.Distance(pos1,pos2);
    }

    void TryShooting()
    {
		if (timeSinceLastShot >= timeBetweenShots) {
			
            if (wallet.GetMoney() >= minimalMoney)
            {
               RayShot(shooter.transform.position + 5* shooter.transform.right);
               RayShot(shooter.transform.position);
               RayShot(shooter.transform.position - 5* shooter.transform.right);
            }
		}
        else
        {
            timeSinceLastShot += Time.deltaTime;
        }

    }

    void RayShot(Vector3 pos)
    {
        RaycastHit hit;
        Ray ray = new Ray(pos, shooter.transform.forward);

        if (Physics.Raycast(ray, out hit))
        {
            //Debug.Log(hit.collider.gameObject.name);
            if (hit.collider.tag=="Character")
            {
                shooter.LaunchMoney();
                timeSinceLastShot = 0;
                UpdateTimeBetweenShots();
            }
        }
    }
}
