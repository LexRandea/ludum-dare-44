﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndPanel : MonoBehaviour
{

    public Button endButton;
    public Text endField;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    public void TimesUp()
    {
        List<MoneyWallet> wallets = new List<MoneyWallet>();

        // check if player is number one
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Character"))
        {
            wallets.Add(obj.GetComponent<MoneyWallet>());
        }

        wallets.Sort((a,b) => (b.GetMoney().CompareTo(a.GetMoney())));

        if (wallets[0].GetComponent<PlayerController>() !=null)
        {
            // player is number one
            endField.text="Victory!";
        }
        else
        {
            // player lost
            endField.text="Defeat";
        }

        endButton.gameObject.SetActive(true);
        endField.gameObject.SetActive(true);   
    }

    public void Bankrupt()
    {
        endField.text="Bankrupt!";
        endButton.gameObject.SetActive(true);
        endField.gameObject.SetActive(true);   
    }
}
