﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotGlowCoin : MonoBehaviour
{

    public GameObject cold;
    public GameObject hot;
    public Money money;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (money.Sweet())
        {
            cold.SetActive(true);
            hot.SetActive(false);
        }
        else
        {
            cold.SetActive(false);
            hot.SetActive(true);
        }
    }
}
