﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This component shoots money
public class MoneyShooter : MonoBehaviour
{
    public MoneyWallet wallet;
    public Money coin;
    public float launchSpeed = 5.0f;
    
    public AudioSource firingSound;

    private Transform allCoins;

    // Start is called before the first frame update
    void Start()
    {
        GameObject obj = GameObject.Find("AllCoins");
        if (obj == null) 
        {
            allCoins = new GameObject("AllCoins").transform;
        } 
        else
        {
            allCoins = obj.transform;
        }
    }

    public void LaunchMoney()
    {
        if (wallet.GetMoney() > 0)
        {
            Money c = Instantiate(coin, transform.position, transform.rotation);
            c.transform.SetParent(allCoins);
            c.GetComponent<Rigidbody>().velocity = Vector3.Normalize(transform.forward) * launchSpeed;
            wallet.TakeMoney(c.value);
            if (firingSound != null)
            {
                firingSound.Play();
            }
        }
    }

}
