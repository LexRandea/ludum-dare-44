﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    private Text text;
    private Image image;

    // Start is called before the first frame update
    void Start()
    {
        text = this.GetComponentInChildren<Text>();
        image = this.GetComponentInChildren<Image>();
    }

    public void SetText(string str)
    {
        text.text = str;
    }

    public void SetColor(Color col)
    {
        image.color = col;
    }
}
