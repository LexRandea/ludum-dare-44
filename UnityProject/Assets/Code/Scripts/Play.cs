﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Play : MonoBehaviour
{
    private float time = 30;
    public float waitTime = 1;

    public void Update()
    {
        if ((Time.timeSinceLevelLoad - time) > waitTime)
        {
            SceneManager.LoadScene(1);
        }
    }
    private void OnTriggerEnter(Collider col)
    {
        time = Time.timeSinceLevelLoad;
    }
}
