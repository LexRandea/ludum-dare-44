﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Money : MonoBehaviour
{
    public int value = 10;
    private bool fountainCoin = false;

    private Rigidbody rb;

    void Start()
    {
        rb=GetComponent<Rigidbody>();
    }

    public void SetFromFountain(bool b)
    {
        fountainCoin = b;
    }

    public bool Sweet()
    {
        // return ( transform.position.y < transform.localScale.x * 1.1 / 2 || fountainCoin ) ;
        return (rb.velocity.magnitude<10 || fountainCoin);
    }

}
