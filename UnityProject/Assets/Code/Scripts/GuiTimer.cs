﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GuiTimer : MonoBehaviour
{
    public Text clockText;
    public CountdownClock clock;

    // Update is called once per frame
    void Update()
    {
        clockText.text = clock.GetTime().ToString();
    }
}
