﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyHitbox : MonoBehaviour
{
    public MoneyWallet wallet;
    public MoneyFountain fountain;
    public int lostMoneyOnHit = 100;

    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnTriggerEnter(Collider col)
    {
        Money money = col.gameObject.GetComponent<Money>();
        if (money != null && !money.Sweet())
        {
            //wallet.AddMoney(money.value);
            //Debug.Log("Hit by hot money");
            fountain.LaunchMoney(money.value*10);
            Destroy(col.gameObject);
        }
    }
}
