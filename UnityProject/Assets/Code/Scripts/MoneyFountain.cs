﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this component spawns money
public class MoneyFountain : MonoBehaviour
{
    public MoneyWallet wallet;
    public Money coin;
    public float launchSpeed = 1.0f;
    private Transform allCoins;

    public AudioSource hitSound;

    //private float timeSinceLastSpawn = 0.0f;
    //public float timeBetweenSpawns = 0.5f;

    private float noiseMagnitude = 10.0f;

    // Start is called before the first frame update
    void Start()
    {
        GameObject obj = GameObject.Find("AllCoins");
        if (obj == null) 
        {
            allCoins = new GameObject("AllCoins").transform;
        } 
        else
        {
            allCoins = obj.transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
  //      timeSinceLastSpawn += Time.deltaTime;
		//if (timeSinceLastSpawn >= timeBetweenSpawns) {
		//	timeSinceLastSpawn -= timeBetweenSpawns;
		//	LaunchMoney();
		//}

    }

    public void LaunchMoney(int amount)
    {
        while (amount > 0 && LaunchCoin())
        {
            amount -= coin.value;
        }
        if ((hitSound != null) && (!hitSound.isPlaying))
        {
            hitSound.Play();
        }
    }

    public bool LaunchCoin()
    {
        if (wallet.GetMoney()>0)
        {
            Money c = Instantiate(coin, this.transform.position, Random.rotation);
            c.transform.SetParent(allCoins);
            c.GetComponent<Rigidbody>().velocity = Vector3.Normalize(transform.up  + GenerateNoise()) * launchSpeed;
            c.SetFromFountain(true);
            wallet.TakeMoney(c.value);
        }
        return wallet.GetMoney() > 0;
    }


    private Vector3 GenerateNoise()
    {
        float noisex = noiseMagnitude * RandomNormal();
        float noisey = noiseMagnitude * RandomNormal();
        return transform.forward * noisex + transform.right * noisey;
    }

    private float RandomNormal()
    {
        // The method requires sampling from a uniform random of (0,1]
        // but Random.NextDouble() returns a sample of [0,1).
        double x1 = 1 - Random.value;
        double x2 = 1 - Random.value;

        double y1 = System.Math.Sqrt(-2.0 * System.Math.Log(x1)) * System.Math.Cos(2.0 * System.Math.PI * x2);
        return (float) y1;
    }

}
