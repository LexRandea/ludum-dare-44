﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountdownClock : MonoBehaviour
{
    public float totalTime;
    private float remainingTime;

    public EndPanel endPanel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        remainingTime = totalTime - Time.timeSinceLevelLoad; // Don't allow negative remaining time
        if (remainingTime<=0)
        {
            endPanel.gameObject.SetActive(true);
            endPanel.TimesUp();
            gameObject.SetActive(false);
        }
        
    }

    public TimeSpan GetTime()
    {
        return new TimeSpan(0, 0, (int) remainingTime);
    }

    public void StopClock()
    {
        remainingTime=0;
    }
}
