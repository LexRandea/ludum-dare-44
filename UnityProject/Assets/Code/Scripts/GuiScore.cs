﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class GuiScore : MonoBehaviour
{

    public List<MoneyWallet> wallets;
    public List<Text> moneyTexts;
    
    // Start is called before the first frame update
    void Start()
    {
        wallets = new List<MoneyWallet>();
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Character"))
        {
            wallets.Add(obj.GetComponent<MoneyWallet>());
        }

        moneyTexts = new List<Text>();
        foreach (Transform child in this.transform)
        {

            moneyTexts.Add(child.GetComponent<Text>());
            moneyTexts[moneyTexts.Count-1].text="";
        }



    }

    // Update is called once per frame
    void Update()
    {
        // List<MoneyWallet> sortedList = wallets.OrderBy(o=>o.money).ToList();
        wallets.Sort((a,b) => (b.GetMoney().CompareTo(a.GetMoney())));
        int K = System.Math.Min(moneyTexts.Count,wallets.Count);
        for (int k=0; k<K; k++)
        {
            moneyTexts[k].text= wallets[k].owner + " : $" + wallets[k].GetMoney().ToString();
        }
        

    }
}
