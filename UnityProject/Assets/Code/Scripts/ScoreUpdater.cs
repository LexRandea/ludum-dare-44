﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreUpdater : MonoBehaviour
{
    public List<MoneyWallet> wallets;
    public List<Score> scores;

    // Start is called before the first frame update
    void Start()
    {
        FindWallets();
        FindScores();
        UpdateScores();
    }

    void FindWallets()
    {
        wallets = new List<MoneyWallet>();
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Character"))
        {
            wallets.Add(obj.GetComponent<MoneyWallet>());
        }
    }

    void FindScores()
    {
        scores = new List<Score>();
        foreach (Transform child in this.transform)
        {

            scores.Add(child.GetComponent<Score>());
        }
    }



    // Update is called once per frame
    void Update()
    {
        UpdateScores();
    }

    void UpdateScores()
    {
        wallets.Sort((a, b) => (b.GetMoney().CompareTo(a.GetMoney())));
        int K = System.Math.Min(scores.Count, wallets.Count);
        for (int k = 0; k < K; k++)
        {
            scores[k].SetText(wallets[k].owner + " : $" + wallets[k].GetMoney().ToString());
            scores[k].SetColor(wallets[k].color);
        }
    }
}
