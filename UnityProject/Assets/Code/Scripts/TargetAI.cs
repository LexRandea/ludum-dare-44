﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;


public class TargetAI : MonoBehaviour {

    public int noise = 10;
    public NavMeshAgent agent;
    public MoneyShooter shooter;
    public Transform target;

    public bool canCheat = true;

    public float timeSinceLastCheat=0.0f;
    public float timeBetweenCheats=30.0f;

    public MoneyWallet wallet;
    public Animator animator;

    private float timeSinceLastShot = 0.0f;
    private float timeBetweenShots = 0.0f;
    public float maxTimeBetweenShots = 0.0f;

    private float timeSinceLastUpdate=0.0f;
    public float timeBetweenUpdates=10.0f;

    private List<MoneyWallet> characters;

    public int minimalMoney = 100;

    public Transform destination;

    void Start()
    {
        Random.InitState((int)(System.DateTime.Now.Second));

        agent.updateRotation = false;

        characters = new List<MoneyWallet>();
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Character"))
        {
            if (obj != this.gameObject)
            {
                characters.Add(obj.GetComponent<MoneyWallet>());
            }
        }
        UpdateTarget();
    }

    void Update()
    {   

        if (agent.speed>1)
        {
            animator.SetBool("Running", true);
        }
        else
        {
            animator.SetBool("Running", false);
        }

        // look at closest opponent
        transform.rotation = Quaternion.LookRotation(GetLookDirection());

        // update destination
        if (timeSinceLastUpdate >= timeBetweenUpdates || DistanceToDestination()<10)
        {
            UpdateDestination();
            timeSinceLastUpdate = 0;
        }  
        else
        {
            timeSinceLastUpdate += Time.deltaTime;
        } 
        
        // try to shoot
		if (timeSinceLastShot >= timeBetweenShots && wallet.GetMoney() >= minimalMoney)
        {
			TryShooting();
		}
        else
        {
            timeSinceLastShot += Time.deltaTime;
        }

        if (canCheat && timeSinceLastCheat>=timeBetweenCheats)
        {
            wallet.AddMoney(Random.Range(50,500));
            timeSinceLastCheat=0;
        }

    }

    Vector3 GetLookDirection()
    {
        /*  // find closest character
        characters.Sort((a,b) => (DistanceToMe(b.gameObject.transform).CompareTo(DistanceToMe(a.gameObject.transform))));
        // look at it
        Vector3 res =characters[characters.Count-1].gameObject.transform.position-transform.position;
        */
        Vector3 res =target.position-transform.position;
        res.Normalize();
        res = Vector3.RotateTowards(transform.forward,res,0.1f,10.0f);
        res.y=0;
        res.Normalize();
        return res;
    }

    float DistanceToMe(Transform tf)
    {
        return (tf.position-transform.position).magnitude;
    }

    void UpdateDestination()
    {
        UpdateTarget();
        Vector3 dist = target.position + (new Vector3(Random.Range(-noise, noise), 0, Random.Range(-noise, noise)));
        if (destination != null)
        {
            destination.position = dist;
        }
        agent.SetDestination(dist);
    }


    void UpdateTarget()
    {
        //characters.Sort((a,b) => (b.GetMoney().CompareTo(a.GetMoney())));
        //target = characters[0].gameObject.transform;
        characters.Sort((a,b) => (DistanceToMe(b.gameObject.transform).CompareTo(DistanceToMe(a.gameObject.transform))));
        target = characters[characters.Count-1].gameObject.transform;
    }

    void UpdateTimeBetweenShots()
    {
        timeBetweenShots = maxTimeBetweenShots * Random.Range(0.0f,1.0f);
    }

    float DistanceToTarget()
    {
        Vector3 pos1 = transform.position;
        pos1.y = 0.0f;

        Vector3 pos2 = target.position;
        pos2.y = 0.0f; 

        return Vector3.Distance(pos1,pos2);
    }

    float DistanceToDestination()
    {
        Vector3 pos1 =  transform.position;
        pos1.y = 0.0f;

        Vector3 pos2 = agent.pathEndPosition;
        pos2.y = 0.0f; 

        return Vector3.Distance(pos1,pos2);
    }

    void TryShooting()
    {
        RayShot(shooter.transform.position + 5* shooter.transform.right);
        RayShot(shooter.transform.position);
        RayShot(shooter.transform.position - 5* shooter.transform.right);
    }

    void RayShot(Vector3 pos)
    {
        RaycastHit hit;
        Ray ray = new Ray(pos, shooter.transform.forward);

        if (Physics.Raycast(ray, out hit))
        {
            //Debug.Log(hit.collider.gameObject.name);
            if (hit.collider.tag=="Character")
            {
                shooter.LaunchMoney();
                animator.SetTrigger("Shooting");
                timeSinceLastShot = 0;
                UpdateTimeBetweenShots();
            }
        }
    }
}
