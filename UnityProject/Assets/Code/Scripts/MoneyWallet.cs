﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// represent the amount of money the player has
public class MoneyWallet : MonoBehaviour
{

    // the amount of money in the wallet
    public string owner;
    public int money = 0;
    public int startMoney = 100;
    public bool disableOnBankruptcy = true;
    public int walletSize = 500;
    public Color color;

    // Start is called before the first frame update
    void Start()
    {
        money = startMoney;
    }


    public int GetMoney()
    {
        return money;
    }

    public bool AddMoney(int amount)
    {
        if ((walletSize<0)|(money<walletSize))
        {
            money = money + amount;

            if (money>walletSize)
            {
                money = walletSize;
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public int TakeMoney(int amount)
    {
        if (money > amount)
        {
            money = money - amount;
            return amount;
        }
        else
        {
            int tmp = money;
            money = 0;
            if (disableOnBankruptcy)
            {
                gameObject.SetActive(false);
            }

            return tmp;
        }
    }

}
