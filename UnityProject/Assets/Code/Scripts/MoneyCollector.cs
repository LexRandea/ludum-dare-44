﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyCollector : MonoBehaviour
{
    public MoneyWallet wallet;
    public AudioSource collectAudioSource;
    public AudioClip[] collectAudioClips = new AudioClip[5];

    // Start is called before the first frame update
    void Start()
    {

    }


    private void OnTriggerEnter(Collider col)
    {
        Money money = col.gameObject.GetComponent<Money>();
        if(money != null && money.Sweet())
        {
            if (wallet.AddMoney(money.value))
            {
//Debug.Log("Collected sweet money");
                Destroy(col.gameObject);
                if ((collectAudioSource != null) && (!collectAudioSource.isPlaying))
                {
                    collectAudioSource.clip = collectAudioClips[(int)Random.Range(0, collectAudioClips.Length)];
                    collectAudioSource.Play();
                }
            }
        }
    }
}
