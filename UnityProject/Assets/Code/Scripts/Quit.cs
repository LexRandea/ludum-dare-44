﻿using UnityEngine;
using System.Collections;

public class Quit : MonoBehaviour
{
    private float time = 1000000; /// Big number
    public float waitTime = 1;

    public void Update()
    {
        if ((Time.timeSinceLevelLoad - time) > waitTime)
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit ();
#endif

        }
    }
    private void OnTriggerEnter(Collider col)
    {
        time = Time.timeSinceLevelLoad;
    }
}